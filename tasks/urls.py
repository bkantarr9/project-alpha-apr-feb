from django.urls import path
from tasks.views import task_list, create_task, show_my_tasks

urlpatterns = [
    path("list/", task_list, name="task_list"),
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
